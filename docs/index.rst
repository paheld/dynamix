.. DynamiX documentation master file, created by
   sphinx-quickstart on Fri Jun  6 10:55:56 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to DynamiX's documentation!
===================================


Contents:

.. toctree::
   :maxdepth: 2
   
   readme
   events
   barabasi


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

