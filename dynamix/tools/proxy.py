__author__ = 'pheld'

import types

"""
Submodule which contains functions and classes to observate objects.
"""


def _get_proxy_function(obj, function_name):
    """
    Proxy function, which is called instead of the original function.

    First the "before_"+function name of the listener is called.
    Then the original functions is called.
    At the end the "after_"function name and the function name itself from the listener is called.
    """
    def fkt(self, *args, **kwargs):
        listener_list = eval("self._proxy._get_listener()")
        for listener in listener_list:
            try:
                exec("listener.before_%s(self, *args, **kwargs)"%(function_name))
            except (AttributeError, TypeError):
                pass
        exec("self.__class__.%s(self, *args, **kwargs)"%(function_name))
        for listener in listener_list:
            try:
                exec("listener.%s(self, *args, **kwargs)"%(function_name))
            except (AttributeError, TypeError):
                pass
            try:
                exec("listener.after_%s(self, *args, **kwargs)"%(function_name))
            except (AttributeError, TypeError):
                pass

    return types.MethodType(fkt, obj)


class ListenerProxy():
    """
    ListenerProxy is a class, which allows the observation of an object.

    It manages listener which are called, if observated methods are called.
    """
    listener = []

    def __init__(self, obj):
        self._obj = obj
        try:
            self.listener = obj._proxy.listener
        except AttributeError:
            obj._proxy = self

        #graph.add_node = types.MethodType(_proxy_add_node, graph)


    def listen_function(self, function_name):
        """
        Creates a new hook for a member function
        """
        fkt = _get_proxy_function(self._obj, function_name)
        exec("self._obj.%s = fkt"%(function_name))

    def _get_listener(self):
        """
        returns the listerner list
        """
        return self.listener


    def add_iterator(self, iterator):
        """
        adds an iterator, which is called if an observated function is called.
        """
        self.listener.append(iterator)


class GraphListenerProxy(ListenerProxy):
    """
    Special Listener Proxy, which automatically observes all graph modification methods in networkx graphs
    """
    def __init__(self, graph):
        ListenerProxy.__init__(self, graph)

        functions = [
            "add_node",
            "add_nodes_from",
            "remove_node",
            "remove_nodes_from",
            "add_edge",
            "add_edges_from",
            "add_path",
            "remove_edge",
            "remove_edges_from",
            "clear"
        ]

        for fkt in functions:
            self.listen_function(fkt)