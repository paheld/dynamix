#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

KEY = "NHC"

import matplotlib
matplotlib.use("TkAgg")

import community
import networkx as nx
from matplotlib import pyplot as plt
from functools import total_ordering
import sys
import json

import logging
logger = logging.getLogger(__name__)
ch = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s -\t%(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
#logger.setLevel(logging.DEBUG)

import heapq
import math
import numpy as np
from sklearn.cluster import spectral_clustering

import time, timeit

from developing.fastcommunity import communityStructureNewman




def draw_pie(ax, ratios=[0.4,0.3,0.3], X=0, Y=0, size=1000):
    N = len(ratios)

    xy = []

    start = 0.
    for ratio in ratios:
        x = [0] + np.cos(np.linspace(2*math.pi*start,2*math.pi*(start+ratio), 30)).tolist()
        y = [0] + np.sin(np.linspace(2*math.pi*start,2*math.pi*(start+ratio), 30)).tolist()
        xy1 = list(zip(x,y))
        xy.append(xy1)
        start += ratio

    pos = 0
    for i, xyi in enumerate(xy):
        ax.scatter([X],[Y] , marker=(xyi,0), s=size, vmin=0, vmax=len(ratios)-1, c=pos)
        pos +=1


def draw_node(node, pos, memberships, size):
    ax = plt.gca()
    xy = pos[node]
    draw_pie(ax,memberships, xy[0], xy[1], size)


@total_ordering
class Massage(object):
    """
    source: label
    target: label
    hub: dict (hub_label:value)
    dist: number
    """
    def __init__(self, source, target, hub, dist, threshold, id):
        self.source = source
        self.target = target
        self.hub = hub
        self.dist = dist
        self.threshold = threshold
        self.id = id

    def __lt__(self, other):
        if self.threshold != other.threshold:
            return self.threshold > other.threshold
        if self.dist != other.dist:
            return self.dist < other.dist
        if self.source != other.source:
            return self.source < other.source
        if self.target != other.target:
            return self.target < other.target
        return self.id < other.id

    def __eq__(self, other):
        return self.dist == other.dist and \
            self.source == other.source and \
            self.target == other.target and \
            self.hub == other.hub

    def __str__(self):
        return "{}->{} {},{} [{}]".format(self.source, self.target, self.dist, self.threshold, self.hub)


def unweighted():
    return lambda _: 1


def weight_similarity(attribute="value"):
    return lambda props: 1/props[attribute] if attribute in props else 1


def weight_distance(attribute="value"):
    return lambda props: props[attribute] if attribute in props else 1


class NHC(object):
    def __init__(self, graph, weight=unweighted(), direction=None):
        self.msg_queue = []
        self.last_message_id = 0
        self.graph = graph
        self.weight = weight
        self.direction = direction
        self.hubs = list()
        self.colors = dict()
        self.message_counter = 0

    def init_nodes(self, min_degree):
        for node in self.graph.nodes():
            self.graph.node[node][KEY] = dict(distance=sys.maxsize)
            if self.graph.degree(node) >= min_degree:
                self.set_state(node, None, 0, {node: 1})
                self.hubs.append(node)
                self.colors[node] = len(self.colors)

    def set_state(self, node, next_node, distance, hubs):
        state = self.graph.node[node][KEY]
        if state["distance"] < distance:
            return

        if state["distance"] > distance:
            state = {
                "distance": distance,
                next_node: hubs
            }
            self.graph.node[node][KEY] = state
        else:
            state[next_node] = hubs
        hubs = self.get_state_hubs(state)
        logger.debug(state)
        for n in nx.all_neighbors(self.graph, node):
            if n == next_node:
                continue
            self.last_message_id += 1
            heapq.heappush(self.msg_queue,
                           Massage(node, n, hubs,
                                   distance+self.weight(self.graph[node][n]),
                                   0, self.last_message_id))

    def remove_edge_notify(self, node, parent):
        state = self.graph.node[node][KEY]
        if parent not in state:
            return
        if len(state) == 1:
            state["distance"] = sys.maxsize
        hubs = self.get_state_hubs(state)
        logger.debug(state)
        for n in nx.all_neighbors(self.graph, node):
            self.last_message_id += 1
            heapq.heappush(self.msg_queue,
                           Massage(node, n, hubs,
                                   state["distance"]+self.weight(self.graph[node][n]),
                                   0, self.last_message_id))

    def add_edge_notify(self, node, other):
        state = self.graph.node[node][KEY]
        hubs = self.get_state_hubs(state)
        logger.debug(state)
        self.last_message_id += 1
        heapq.heappush(self.msg_queue,
                       Massage(node, other, hubs,
                               state["distance"]+self.weight(self.graph[node][other]),
                               0, self.last_message_id))

    def get_state_hubs(self, state):
        clustering = dict()
        for next_node, next_node_state in state.items():
            if next_node in ("distance",):
                continue
            for cluster, value in next_node_state.items():
                if cluster in clustering:
                    clustering[cluster] += value
                else:
                    clustering[cluster] = value
        s = sum(clustering.values())
        for cluster in clustering:
            clustering[cluster] /= s
        #logger.debug(clustering)
        return clustering

    def get_hubs_dict(self, node):
        state = self.graph.node[node][KEY]
        return self.get_state_hubs(state)

    def get_hubs_list(self, node):
        state = self.get_hubs_dict(node)
        return [state.get(h, 0) for h in self.hubs]

    def get_next_nodes(self, node):
        return [n for n in self.graph.node[node][KEY].keys() if n not in ("Distance",)]

    def get_crisp_cluster(self, node):
        try:
            return max((val, hub) for hub, val in self.get_hubs_dict(node).items())[1]
        except ValueError:
            return -1

    def get_distance(self, node):
        return self.graph.node[node][KEY]["distance"]

    def process_all_messages(self):
        while self.msg_queue:
            self.process_message(heapq.heappop(self.msg_queue))

    def process_message(self, message):
        self.message_counter += 1
        logger.debug(message)
        self.set_state(message.target, message.source, message.dist, message.hub)

    def get_message_counter(self):
        return self.message_counter

    def reset_message_counter(self):
        self.message_counter = 0


def estimate_threshold(histogram, min_clusters):
    s = 0
    for i in range(len(histogram)-1, -1, -1):
        s += histogram[i]
        if s >= min_clusters:
            return i
    return 0


def simple_test():
    dataset = "karate"
    min_degree = 16

    #dataset = "dolphins"
    #min_degree = 11

    #dataset = "football"
    #min_degree = 12

    #dataset = "netscience"
    #min_degree = 8

    #dataset = "lesmiserables"
    #dataset = "barabasi_1000_4"
    #min_degree = 40


    g = nx.read_gexf("../../datasets/{}/{}-core.gexf".format(dataset, dataset)) # 11
    with open("../../datasets/{}/meta-core.json".format(dataset)) as file:
        meta = json.load(file)

    logger.info("Loaded Dataset %s", dataset)
    print("Louvain number of cluster:", meta["louvain_clustering"]["number_of_clusters"])
    histogram = nx.degree_histogram(g)
    print(meta)
    min_degree = estimate_threshold(histogram, meta["louvain_clustering"]["number_of_clusters"])
    #min_degree = 16
    print("Est. min-degree:", min_degree)
    print("Histogram:", histogram)

    nhc = NHC(g, weight=unweighted())
    time_start = time.process_time()
    nhc.init_nodes(min_degree)
    time_init = time.process_time() - time_start
    nhc.process_all_messages()
    time_nhc = time.process_time() - time_init

    for n in g.nodes():
        logger.info("Result: %s\t%s\t%s", n, nhc.get_crisp_cluster(n), nhc.get_hubs_list(n))

    nodelist = list(g.nodes())
    partition = {n: nhc.get_crisp_cluster(n) for n in nodelist}
    clusterlist = [partition[n] for n in nodelist]
    colorlist = [nhc.colors.get(c, -1) for c in clusterlist]
    colorlist_louvain = [meta["louvain_clustering"]["partition"].get(str(n), -1) for n in nodelist]

    edgelist = list(g.edges())
    edgelist_active = set([e for e in edgelist if (e[1] in nhc.get_next_nodes(e[0]) or (e[0] in nhc.get_next_nodes(e[1])))])
    edgelist_other = [e for e in edgelist if e not in edgelist_active]

    time_start = time.process_time()
    modularity = community.modularity(partition, g)
    time_mod = time.process_time() - time_start

    print("Louvain modularity: ", meta["louvain_clustering"]["modularity"])
    print("NextHub modularity: ", modularity)

    pos = meta["pos_spring"]

    time_start = time.process_time()
    plt.figure()
    nx.draw_networkx_edges(g, pos, edgelist=edgelist_active, width=2)
    nx.draw_networkx_edges(g, pos, edgelist=edgelist_other, style="dotted")
    for node in g.nodes():
        draw_node(node, pos, nhc.get_hubs_list(node), 150 * (0.7 ** nhc.get_distance(node)))
    nx.draw_networkx_nodes(g, pos, nodelist=nodelist, node_color=colorlist, node_size=80)

    #nx.draw_networkx_labels(g, pos, nodelist=nodelist, font_color="w")
    time_plot = time.process_time() - time_start


    #plt.figure()

    #nx.draw_networkx_edges(g, pos, edgelist=edgelist)
    #nx.draw_networkx_nodes(g, pos, nodelist=nodelist, node_color=colorlist_louvain, node_size=80)

    plt.show()

    print("Time Init:      ", round(time_init*1000, 3), "ms")
    print("Time Cluster:   ", round(time_nhc*1000, 3), "ms")
    print("Time Modularity:", round(time_mod*1000, 3), "ms")
    print("Time Plot:      ", round(time_plot*1000, 3), "ms")


def analyze_dataset(dataset, weight=unweighted(), result_plots=True, graph_plots=False, iterations=100, max_cluster=None):
    g = nx.read_gexf("../../datasets/{}/{}-core.gexf".format(dataset, dataset)) # 11
    with open("../../datasets/{}/meta-core.json".format(dataset)) as file:
        meta = json.load(file)

    histogram = nx.degree_histogram(g)
    print("Degree Distribution:")
    for i in range(len(histogram)):
        print(i, histogram[i])

    if result_plots:
        plt.figure()
        plt.title("{} - Node Degree Distribution".format(dataset))
        plt.xlabel("Degree")
        plt.ylabel("Number of Nodes")
        plt.bar(range(0, len(histogram)), histogram)

    times = list()
    modularities = list()
    clusters = list()

    degrees = list(range(1, len(histogram)))
    nodelist = list(g.nodes())

    best_mod = -1
    best_degree = 0

    print("Degree\tCluster\tModularity\tTime")
    clusterVSmodularity = {}

    for i in degrees:
        g_copy = g.copy()
        t_total = 0
        for _ in range(iterations):
            nhc = NHC(g_copy, weight=weight)
            time_start = time.process_time()
            nhc.init_nodes(i)
            nhc.process_all_messages()
            t_total += time.process_time() - time_start
        t = round(t_total / iterations * 1000, 3)
        partition = {n: nhc.get_crisp_cluster(n) for n in nodelist}
        modularity = round(community.modularity(partition, g_copy), 5)
        number_of_cluster = len(set(partition.values()))
        print("{}\t\t{}\t\t{}\t\t{} ms".format(i, number_of_cluster, modularity, t))
        times.append(t)
        modularities.append(modularity)
        clusters.append(number_of_cluster)
        clusterVSmodularity[number_of_cluster] = modularity
        if modularity > best_mod:
            best_mod = modularity
            best_degree = i

    nhc_cVm_cluster = sorted(clusterVSmodularity.keys())
    nhc_cVm_modularity = [clusterVSmodularity[c] for c in nhc_cVm_cluster]

    min_degree_est = estimate_threshold(histogram, meta["louvain_clustering"]["number_of_clusters"])
    min_degree = best_degree

    nodelist = sorted(list(g.nodes()))

    if not max_cluster:
        max_cluster = max(nhc_cVm_cluster)
    else:
        max_cluster = min(max(nhc_cVm_cluster), max_cluster)

    adjM = nx.to_numpy_matrix(g, nodelist)

    spec_cVm_cluster = list(range(2, max_cluster))
    #spec_cVm_cluster = nhc_cVm_cluster[:-1]
    spec_cVm_modularity = list()
    spec_cVm_modularity_disc = list()

    print("Cluster - Modularity:")
    print("#Cluster\tNHC\t\tSpec (k-means)\tSpec (discretize)\tNewman\tLouvain")

    louvain_cVm = {}

    louvain_dendrogram = community.generate_dendrogram(g)
    for level in range(len(louvain_dendrogram)):
        part = community.partition_at_level(louvain_dendrogram, level)
        number_of_cluster = len(set(part.values()))
        modularity = community.modularity(part, g)
        louvain_cVm[number_of_cluster] = modularity
    louvain_cVm_cluster = sorted(list(louvain_cVm.keys()))
    louvain_cVm_modularity = [louvain_cVm[k] for k in louvain_cVm_cluster]

    for c in spec_cVm_cluster:
        spec_labels = spectral_clustering(adjM, n_clusters=c)
        spec_part = {n: l for n,l in zip(nodelist, spec_labels)}
        modularity_spec = round(community.modularity(spec_part, g), 5)
        spec_cVm_modularity.append(modularity_spec)

        spec_labels = spectral_clustering(adjM, n_clusters=c, assign_labels='discretize')
        spec_part = {n: l for n,l in zip(nodelist, spec_labels)}
        modularity_spec_disc = round(community.modularity(spec_part, g), 5)
        spec_cVm_modularity_disc.append(modularity_spec_disc)

        print("{}\t\t\t{}\t{}\t\t\t{}\t\t\t\t{}\t{}".format(c, clusterVSmodularity.get(c,"-\t"),
                                                  modularity_spec, modularity_spec_disc,
                                                  "-\t", louvain_cVm.get(c,"-")))

    if result_plots:
        plt.figure()
        plt.title("{} - NHC Process Time".format(dataset))
        plt.xlabel("Min Degree")
        plt.ylabel("Process Time [ms]")
        plt.plot(degrees, times)

        plt.figure()
        plt.title("{} - Modularity".format(dataset))
        plt.xlabel("Min Degree")
        plt.ylabel("Modularity")
        plt.plot(degrees, modularities)
        plt.plot(degrees, [meta["louvain_clustering"]["modularity"]] * len(degrees))
        plt.legend(("NHC", "Louvain"))

        plt.figure()
        plt.title("{} - Number of Clusters".format(dataset))
        plt.xlabel("Min Degree")
        plt.ylabel("Number of Clusters")
        plt.plot(degrees, clusters)
        plt.plot(degrees, [meta["louvain_clustering"]["number_of_clusters"]] * len(degrees))

        plt.figure()
        plt.title("{} - Number of Clusters vs. Modularity".format(dataset))
        plt.xlabel("# clusters")
        plt.ylabel("Modularity")
        nhc_cVm_cluster = [x for x in nhc_cVm_cluster if x <= max_cluster]
        nhc_cVm_modularity = nhc_cVm_modularity[0:len(nhc_cVm_cluster)]
        plt.scatter(nhc_cVm_cluster, nhc_cVm_modularity, marker="*", color="b")
        plt.scatter(spec_cVm_cluster, spec_cVm_modularity, color='r')
        plt.scatter(spec_cVm_cluster, spec_cVm_modularity_disc, marker='+', color='r')
        louvain_cVm_cluster = [x for x in louvain_cVm_cluster if x <= max_cluster]
        louvain_cVm_modularity = louvain_cVm_modularity[0:len(louvain_cVm_cluster)]
        plt.scatter(louvain_cVm_cluster, louvain_cVm_modularity, marker='^', color='g')
        plt.legend(["NHC", "Spectral - kmeans", "Spectral - discret", "Louvain"], loc="lower right")


    print("Louvain modularity: ", meta["louvain_clustering"]["modularity"])
    print("NextHub modularity: ", best_mod)
    print("NextHub Min-Degree: ", best_degree)

    print("Fastcommunity")
    fst = communityStructureNewman(g)
    print("Fastgreed Modularity: ", fst[0])
    print("Fastgreed Partition: ", list(fst[1]))
    print("Fastgreed ..: ", list(fst[2]))
    print("Fastgreed ..: ", list(fst[3]))
    for i, val in enumerate(fst[2]):
        print(i, val)

    if graph_plots:
        plt.figure()
        nhc = NHC(g, weight=unweighted())
        nhc.init_nodes(min_degree)
        nhc.process_all_messages()

        for n in g.nodes():
            logger.info("Result: %s\t%s\t%s", n, nhc.get_crisp_cluster(n), nhc.get_hubs_list(n))


        partition = {n: nhc.get_crisp_cluster(n) for n in nodelist}
        clusterlist = [partition[n] for n in nodelist]
        colorlist = [nhc.colors.get(c, -1) for c in clusterlist]
        colorlist_louvain = [meta["louvain_clustering"]["partition"].get(str(n), -1) for n in nodelist]

        edgelist = list(g.edges())
        edgelist_active = set([e for e in edgelist if (e[1] in nhc.get_next_nodes(e[0]) or (e[0] in nhc.get_next_nodes(e[1])))])
        edgelist_other = [e for e in edgelist if e not in edgelist_active]

        modularity = community.modularity(partition, g)

        pos = meta["pos_spring"]

        nx.draw_networkx_edges(g, pos, edgelist=edgelist_active, width=2)
        nx.draw_networkx_edges(g, pos, edgelist=edgelist_other, style="dotted")
        for node in g.nodes():
            draw_node(node, pos, nhc.get_hubs_list(node), 150 * (0.7 ** nhc.get_distance(node)))
        nx.draw_networkx_nodes(g, pos, nodelist=nodelist, node_color=colorlist, node_size=80)

        #nx.draw_networkx_labels(g, pos, nodelist=nodelist, font_color="w")
        plt.axes().set_xticklabels([])
        plt.axes().set_yticklabels([])


        plt.figure()

        nx.draw_networkx_edges(g, pos, edgelist=edgelist)
        nx.draw_networkx_nodes(g, pos, nodelist=nodelist, node_color=colorlist_louvain, node_size=80)
        plt.axes().set_xticklabels([])
        plt.axes().set_yticklabels([])

    plt.show()


def do_dynamic_test_remove():
    # g = nx.powerlaw_cluster_graph(1000, 10, 0.7)
    # histogram = nx.degree_histogram(g)
    # print("Degree Distribution:")
    # for i in range(len(histogram)):
    #     print(i, histogram[i])
    #
    # if True:
    #     plt.figure()
    #     plt.title("Node Degree Distribution")
    #     plt.xlabel("Degree")
    #     plt.ylabel("Number of Nodes")
    #     plt.bar(range(0, len(histogram)), histogram)
    #
    # times = list()
    # modularities = list()
    # clusters = list()
    #
    # degrees = [1+x for x,y in enumerate(histogram) if y>0] #list(range(1, len(histogram)))
    # nodelist = list(g.nodes())
    #
    # best_mod = -1
    # best_degree = 0
    #
    # print("Degree\tCluster\tModularity\tTime")
    # clusterVSmodularity = {}
    #
    # if False:
    #     for i in degrees:
    #         g_copy = g.copy()
    #         nhc = NHC(g)
    #         nhc.init_nodes(i)
    #         nhc.process_all_messages()
    #         partition = {n: nhc.get_crisp_cluster(n) for n in nodelist}
    #         modularity = round(community.modularity(partition, g_copy), 5)
    #         number_of_cluster = len(set(partition.values()))
    #         print("{}\t\t{}\t\t{}".format(i, number_of_cluster, modularity))
    #         modularities.append(modularity)
    #         clusters.append(number_of_cluster)
    #         clusterVSmodularity[number_of_cluster] = modularity
    #         if modularity > best_mod:
    #             best_mod = modularity
    #             best_degree = i

    #print(best_mod, best_degree)
    #part = community.best_partition(g)
    #print(community.modularity(g, part))
    #125

    #print("start")
    removes = []
    for i in range(100):
        print("Iteration",i)
        g = nx.powerlaw_cluster_graph(1000, 10, 0.7)
        nhc = NHC(g)
        nhc.init_nodes(125)
        nhc.process_all_messages()
        partition_nhc = {n: nhc.get_crisp_cluster(n) for n in g.nodes()}
        #print("NHC")
        #print("Communities:", len(set(partition_nhc.values())))
        #print(community.modularity(partition_nhc, g))
        #print("Init-Messages:", nhc.message_counter)
        nhc.reset_message_counter()

        edges = set(g.edges())
        import random
        for edge in random.sample(edges, 100):
            g.remove_edge(edge[0], edge[1])
            nhc.remove_edge_notify(edge[0], edge[1])
            nhc.remove_edge_notify(edge[1], edge[0])
            nhc.process_all_messages()
            print(edge, "Remove Messages:", nhc.message_counter)
            removes.append(nhc.message_counter)
            nhc.reset_message_counter()
    plt.figure()
    plt.hist(removes, 25, log=True)
    print(removes)
    print("Avg:", sum(removes)/len(removes))
    removes.sort()
    print("Median:", removes[int(len(removes)/2)])
    print("Max:", removes[-1])
    print("0-messages:", len([x for x in removes if x==0]))
    plt.show()
    #print("\n")

    #print("louvain")
    #part = community.best_partition(g)
    #print("Communities:", len(set(part.values())))
    #print(community.modularity(part, g))


def do_dynamic_test_add():
    adds = []
    for i in range(100):
        print("Iteration",i)
        g = nx.powerlaw_cluster_graph(1000, 10, 0.7)
        nhc = NHC(g)
        nhc.init_nodes(125)
        nhc.process_all_messages()
        partition_nhc = {n: nhc.get_crisp_cluster(n) for n in g.nodes()}
        #print("NHC")
        #print("Communities:", len(set(partition_nhc.values())))
        #print(community.modularity(partition_nhc, g))
        #print("Init-Messages:", nhc.message_counter)
        nhc.reset_message_counter()

        nodes = set(g.nodes())
        import random
        for _ in range(100):
            ok=False
            while not ok:
                edge = random.sample(nodes, 2)
                ok = not g.has_edge(edge[0], edge[1])
            g.add_edge(edge[0], edge[1])
            nhc.add_edge_notify(edge[0], edge[1])
            nhc.add_edge_notify(edge[1], edge[0])
            nhc.process_all_messages()
            print(edge, "Add Messages:", nhc.message_counter)
            adds.append(nhc.message_counter)
            nhc.reset_message_counter()
    plt.figure()
    plt.hist(adds, 25, log=True)
    print(adds)
    print("Avg:", sum(adds)/len(adds))
    adds.sort()
    print("Median:", adds[int(len(adds)/2)])
    print("Max:", adds[-1])
    print("2-messages:", len([x for x in adds if x==0]))
    plt.show()


if __name__ == "__main__":
    with plt.xkcd():
        simple_test()
    #analyze_dataset("barabasi_1000_4", iterations=1, result_plots=True)
        analyze_dataset("karate", iterations=1, result_plots=True, graph_plots=True, max_cluster=2)
    #do_dynamic_test_remove()
    #do_dynamic_test_add()