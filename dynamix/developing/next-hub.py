#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

import community
import networkx as nx
from matplotlib import pyplot as plt
from functools import total_ordering

import heapq
import math
import numpy as np

def draw_pie(ax, ratios=[0.4,0.3,0.3], X=0, Y=0, size = 1000):
    N = len(ratios)

    xy = []

    start = 0.
    for ratio in ratios:
        x = [0] + np.cos(np.linspace(2*math.pi*start,2*math.pi*(start+ratio), 30)).tolist()
        y = [0] + np.sin(np.linspace(2*math.pi*start,2*math.pi*(start+ratio), 30)).tolist()
        xy1 = list(zip(x,y))
        xy.append(xy1)
        start += ratio

    pos = 0
    for i, xyi in enumerate(xy):
        ax.scatter([X],[Y] , marker=(xyi,0), s=size, vmin=0, vmax=len(ratios), c=pos)
        pos +=1


@total_ordering
class Massage(object):
    def __init__(self, source, target, hub, dist):
        self.source = source
        self.target = target
        self.hub = hub
        self.dist = dist

    def __lt__(self, other):
        if self.dist != other.dist:
            return self.dist < other.dist
        if self.source != other.source:
            return self.source < other.source
        if self.target != other.target:
            return self.target < other.target
        if self.hub != other.hub:
            return self.hub < other.hub

    def __eq__(self, other):
        return self.dist == other.dist and self.source == other.source and self.target == other.target and self.hub == other.hub

    def __str__(self):
        return "{}->{} {} [{}]".format(self.source, self.target, self.dist, self.hub)


class State(object):
    def __init__(self, hub, dist, next_node):
        self.hub = hub
        self.dist = dist
        self.next_node = next_node

    def __lt__(self, other):
        return self.dist < other.dist

    def __eq__(self, other):
        return self.dist == other.dist and self.hub == other.hub and self.next_node == other.next_node

    def __hash__(self):
        return self.__str__().__hash__()

    def __str__(self):
        return "#{} ({}) [{}]".format(self.hub, self.dist, self.next_node)


class NHC(object):
    def __init__(self, graph):
        self.msg_queue = []
        self.graph = graph
        self.colors = {}

    def init_nodes(self, min_degree):
        #inf_state = State(-1, float("inf"), None)
        for node in self.graph.nodes():
            #self.graph.node[node]["state"] = inf_state
            if self.graph.degree(node) >= min_degree:
                self.set_hub(node)
            else:
                self.set_hub(node, False)

    def set_hub(self, node, is_hub=True):
        if is_hub:
            self.graph.node[node]["state"] = State(node, 0, None)
            self.graph.node[node]["states"] = [State(node, 0, None)]
            color = len(self.colors)
            self.graph.node[node]["color"] = color
            self.colors[node] = color

            for n in nx.all_neighbors(self.graph, node):
                heapq.heappush(self.msg_queue, Massage(node, n, node, 1))

    def process_all_msgs(self):
        while self.msg_queue:
            self.process_msg(heapq.heappop(self.msg_queue))

    def process_msg(self, msg):
        print("Process:",msg)
        if not "state" in self.graph.node[msg.target] or self.graph.node[msg.target]["state"].dist >= msg.dist:
            s = State(msg.hub, msg.dist, msg.source)
            if not "state" in self.graph.node[msg.target] or self.graph.node[msg.target]["state"].dist > msg.dist:

                self.graph.node[msg.target]["states"] = set([s])

            else:
                self.graph.node[msg.target]["states"].add(s)

            hubs = {}
            for s in self.graph.node[msg.target]["states"]:
                if s.hub in hubs:
                    hubs[s.hub][0] += 1
                else:
                    hubs[s.hub] = [1, s]
            hubs = [(val[0], val[1]) for key, val in hubs.items()]
            hubs.sort(reverse=True)
            hub = hubs[0][1].hub
            self.graph.node[msg.target]["state"] = hubs[0][1]
            color_sum = 0
            color_cnt = 0
            for hub_tuple in hubs:
                color_sum += hub_tuple[0] * self.colors[hub_tuple[1].hub]
                color_cnt += hub_tuple[0]
            color = color_sum/color_cnt
            color = self.colors[hub]
            self.graph.node[msg.target]["color"] = color

            for n in nx.all_neighbors(self.graph, msg.target):
                if n == msg.source:
                    continue
                heapq.heappush(self.msg_queue, Massage(msg.target, n, msg.hub, msg.dist+1))

    def get_class_memberships(self, node):
        m = [0 for _ in range(len(self.colors))]
        cnt = 0
        if "states" in self.graph.node[node]:
            print("CALC")
            for s in self.graph.node[node]["states"]:
                print(s)
                m[self.colors[s.hub]] += 1
                cnt +=1
            print(m)
            return [x / cnt for x in m]
        return m

    def make_crisp(self):
        for node in self.graph.nodes():
            m_self = self.get_class_memberships(node)
            m_sum = [0 for _ in range(len(m_self))]
            for n in nx.all_neighbors(self.graph, node):
                m_other = self.get_class_memberships(n)
                for i in range(len(m_self)):
                    m_sum[i] += m_self[i] * m_other[i]
            max_m = -1
            max_idx = 0
            for i in range(len(m_self)):
                if m_sum[i] > max_m:
                    max_m = m_sum[i]
                    max_idx = i
            self.graph.node[node]["cluster"] = max_idx


def draw_node(node, pos, memberships, size):
    ax = plt.gca()
    xy = pos[node]
    draw_pie(ax,memberships, xy[0], xy[1], size)


def analyze(graph, nhc=None):
    for node in graph.nodes(data=True):
        print(node, node[1]["state"], graph.degree(node[0]))


if __name__ == "__main__":
    g = nx.read_gml("../../datasets/dolphins/dolphins.gml") # 11
    #g = nx.read_gml("../../datasets/netscience/netscience.gml")
    #g = nx.read_gml("../../datasets/karate/karate.gml") # 11/16
    #g = nx.read_gml("../../datasets/football/football.gml") # 12
    #g = nx.barabasi_albert_graph(1000,3,0)

    print(nx.degree_histogram(g))

    nhc = NHC(g)
    nhc.init_nodes(11)
    nhc.process_all_msgs()

    nodelist = list(g.nodes())

    for node in nodelist:
        if not "state" in g.node[node]:
            g.remove_node(node)

    analyze(g, nhc)

    for node in g.nodes():
        print(node,  nhc.get_class_memberships(node))


    nodelist = list(g.nodes())

    nhc.make_crisp()

    sizelist = [300 * (0.7 ** g.node[n]["state"].dist) for n in nodelist]

    pos = nx.spring_layout(g, weight=None, iterations=1000)
    #pos = nx.circular_layout(g)
    #pos = nx.fruchterman_reingold_layout(g)

    ## draw value werte
    #colorlist = [g.node[n]["value"] for n in nodelist]
    #nx.draw_networkx_nodes(g, pos, nodelist=nodelist, node_color=colorlist, node_size=300)
    colorlist = [g.node[n]["cluster"] for n in nodelist]
    #nx.draw_networkx_nodes(g, pos, nodelist=nodelist, node_color=colorlist, node_size=100)
    ## end draw



    edgelist = list(g.edges())
    edgelist_active = set([e for e in edgelist if (any([s.next_node == e[1] for s in g.node[e[0]]["states"]]) or any([s.next_node == e[0] for s in g.node[e[1]]["states"]]))])
    edgelist_other = [e for e in edgelist if e not in edgelist_active]
    #print(edgelist)
    nx.draw_networkx_edges(g, pos, edgelist= edgelist_active, width=2)
    nx.draw_networkx_edges(g, pos, edgelist= edgelist_other, style="dotted")

    #nx.draw(g)
    ax = plt.subplot(111)

    for node in g.nodes():
        draw_node(node, pos, nhc.get_class_memberships(node), 150 * (0.7 ** g.node[node]["state"].dist))

    nx.draw_networkx_nodes(g, pos, nodelist=nodelist, node_color=colorlist, node_size=80)

    #matrix = nx.to_scipy_sparse_matrix()

    part = {node: g.node[node]["cluster"] for node in g.nodes()}
    print(part)
    print(community.modularity(part, g))
    part_best = community.best_partition(g)
    print(community.modularity(part_best, g))


    plt.show()