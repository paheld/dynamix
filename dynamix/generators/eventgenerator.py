#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, unicode_literals, absolute_import, generators

import random

from dynamix.events.tools import event_to_string, simplify, save, load


class Generator(object):
    """Generatorklasse, die Eventerzeugung implementiert

    Die Klasse sollte nur die events generieren und nichts weiter an config dateien parsen.
    Zu konfigurationszwecken sollten weitere Funktionen hinzugefügt werden, die dann beispielsweise das
    verschieben von Knoten als Konfiguration aufnimmt.
    """

    def generate(self, maxtime=10):
        """Generatorfunktion, arbeitet mit yield statt return

        Der Code ist nur ein Beispiel was so zurückgegeben werden könnte.

        Jedes yield gibt ein Dictonary mit Listen oder Einzelelementen für Sender und Receiver zurück
        """
        items = list(range(25))

        for t in range(maxtime):
            yield {
                "time": t,
                "sender": list({random.choice(items), random.choice(items), random.choice(items)}),
                "receiver": list({random.choice(items), random.choice(items), random.choice(items)}),
            }


    def __iter__(self):
        """returns the iterator from the standard generate function without optional parameters"""
        return self.generate()





if __name__ == "__main__":

    # some examples ...
    gen = Generator()

    # Use the generate function.
    for event in gen.generate(100):
        print(event)
        print(event_to_string(event))

    # simplify the output
    for event in simplify(gen.generate()):
        print(event)
        print(event_to_string(event))

    # use the object direct as an iterable
    for event in gen:
        print(event)

    # save results to an file
    save(gen.generate(), "test.csv")

    print("LOAD")
    for event in load("test.csv"):
        print(event)


    #################
    #
    # The command line interface stuff comes here ...
    #
    #################
