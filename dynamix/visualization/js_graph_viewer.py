#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

import tornado.ioloop
import tornado.web
import tornado.websocket
import json
import os
import time

from threading import Thread, Lock
from queue import Queue

from dynamix.tools.proxy import GraphListenerProxy

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"


class MainHandler_(tornado.web.RequestHandler):
    def get(self):
        self.redirect("/graph.html")


class WebSocketHandler_(tornado.websocket.WebSocketHandler):
    msgs = Queue()

    # the client connected
    def open(self):
        self.application.connections.add(self)
        if len(self.application.connections) == 1:
            self.application.wait_for_user_lock.release()

        print("New client connected")
        nodes = list(self.application.graph.nodes())
        if nodes:
            self.add_message(json.dumps({"action": "add_node", "nodes": nodes}))
        edges = list(self.application.graph.edges())
        if edges:
            self.add_message(json.dumps({"action": "add_link", "links": edges}))
        t = Thread(target=self.stream_messages)
        t.daemon = True
        t.start()

    def stream_messages(self):
        while True:
            msg = self.msgs.get(block=True)
            self.write_message(msg)

    # the client sent the message
    def on_message(self, message):
        self.write_message(message)

    # client disconnected
    def on_close(self):
        print("Client disconnected")
        self.application.connections.remove(self)
        if len(self.application.connections) == 0:
            self.application.wait_for_user_lock.acquire()

    def add_message(self, msg):
        self.msgs.put(msg)



class JS_graph_viewer():
    thread = None

    def __init__(self, graph, port=8888):
        self.port = port
        self.graph = graph
        p = GraphListenerProxy(graph)
        p.add_iterator(self)

    def run(self):

        self.application = tornado.web.Application([
            (r"/", MainHandler_),
            (r'/websocket/', WebSocketHandler_),
            (r"/(.+)", tornado.web.StaticFileHandler,
                {"path": os.path.dirname(__file__) + os.path.sep + "resources"})
        ])

        self.application.connections = set()
        self.application.wait_for_user_lock = Lock()
        self.application.wait_for_user_lock.acquire()
        self.application.graph = self.graph

        self.application.listen(self.port)

        tornado.ioloop.IOLoop.instance().start()

    def start(self):
        self.thread = Thread(target=self.run)
        self.thread.start()

    def stop(self):
        tornado.ioloop.IOLoop.instance().stop()

    def wait_for_connection(self):
        self.application.wait_for_user_lock.acquire(True)
        self.application.wait_for_user_lock.release()

    def add_message(self, msg):
        for connection in self.application.connections:
            connection.add_message(msg)

    def demonize(self):
        self.thread.daemon = True

    def add_node(self, graph, node, *args, **kwargs):
        self.add_message(json.dumps({"action": "add_node", "nodes": node}))

    def add_nodes_from(self, graph, nodes, *args, **kwargs):
        self.add_message(json.dumps({"action": "add_node", "nodes": list(nodes)}))

    def remove_node(self, graph, node, *args, **kwargs):
        self.add_message(json.dumps({"action": "remove_node", "nodes": node}))

    def remove_nodes_from(self, graph, nodes, *args, **kwargs):
        self.add_message(json.dumps({"action": "remove_node", "nodes": list(nodes)}))

    def add_edge(self, graph, u,v, *args, **kwargs):
        self.add_message(json.dumps({"action": "add_link", "links": (u, v)}))

    def add_edges_from(self, graph, edges, *args, **kwargs):
        edges = list(edges)
        if edges:
            self.add_message(json.dumps({"action": "add_link", "links": edges}))

    def remove_edge(self, graph, u,v, *args, **kwargs):
        self.add_message(json.dumps({"action": "remove_link", "links": (u, v)}))

    def remove_edges_from(self, graph, edges, *args, **kwargs):
        edges = list(edges)
        if edges:
            self.add_message(json.dumps({"action": "remove_link", "links": edges}))

    def add_path(self, graph, path, *args, **kwargs):
        edges = []
        for i in range(len(path)-1):
            edges.append((path[i], path[i+1]))
        print(edges)
        self.add_message(json.dumps({"action": "add_link", "links": edges}))

    def clear(self, *args, **kwargs):
        self.add_message(json.dumps({"action": "clear"}))


if __name__ == "__main__":
    events = [
            {"action": "add_node", "nodes": 4},
            #{"action": "clear"},
            {"action": "add_node", "nodes": [1, 2, 3]},
            {"action": "remove_node", "nodes": 4},
            {"action": "add_link", "links": (1,2)},
            {"action": "add_link", "links": [(2,3),(1,3)]},
            {"action": "remove_link", "links": (1,2)},
        ]

    import networkx as nx

    g = nx.Graph()


    Viewer = JS_graph_viewer(g)
    Viewer.start()
    Viewer.wait_for_connection()
    g.add_node("A")
    g.clear()
    g.add_nodes_from([1,2,3,4,5])
    g.add_path([1,2,3])
    #g.add_path([1,2,3])
    g.add_node(1)

    print(g.nodes())
    print(g.edges())
    time.sleep(1)
    #for e in events:
    #    Viewer.add_message(json.dumps(e))
