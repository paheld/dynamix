// Generated by CoffeeScript 1.7.1
(function() {
  var add_link, add_node, changed, clear, color, force, height, link, links, links_dict, log, node, nodes, nodes_dict, redraw, remove_link, remove_node, start, svg, tick, vis, width, ws;

  width = 1200;

  height = 1000;

  color = d3.scale.category10();

  nodes = [];

  nodes_dict = Object();

  links = [];

  links_dict = Object();

  tick = function() {
    node.attr("transform", function(d) {
      return "translate(" + d.x + "," + d.y + ")";
    });
    return link.attr("x1", function(d) {
      return d.source.x;
    }).attr("y1", function(d) {
      return d.source.y;
    }).attr("x2", function(d) {
      return d.target.x;
    }).attr("y2", function(d) {
      return d.target.y;
    });
  };

  force = d3.layout.force().nodes(nodes).links(links).charge(-400).linkDistance(120).size([width, height]).on("tick", tick);

  redraw = function() {
    return vis.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")");
  };

  svg = d3.select("body").append("svg").attr("width", width).attr("height", height).call(d3.behavior.zoom().on("zoom", redraw));

  log = d3.select("body").append("ul");

  vis = svg.append("g");

  node = vis.selectAll(".node");

  link = vis.selectAll(".link");

  changed = false;

  start = function() {
    var g;
    if (changed) {
      changed = false;
      link = link.data(force.links(), function(d) {
        return d.source.id + "-" + d.target.id;
      });
      link.enter().insert("line", ".node").attr("class", "link");
      link.exit().remove();
      node = node.data(force.nodes(), function(d) {
        return d.id;
      });
      g = node.enter().append("g").attr("class", function(d) {
        return "node c" + d.cluster;
      });
      g.append("circle").attr("r", 8);
      g.append("text").attr("class", "node-label").attr("dx", 12).attr("dy", ".35em").text(function(d) {
        if (d.label) {
          return d.label;
        } else {
          return d.id;
        }
      });
      node.exit().remove();
      return force.start();
    }
  };

  window.setInterval(start, 100);

  add_node = function(list) {
    var n, obj, _i, _len;
    if (!(list instanceof Array)) {
      list = [list];
    }
    for (_i = 0, _len = list.length; _i < _len; _i++) {
      n = list[_i];
      if (!nodes_dict[n]) {
        obj = {
          id: n,
          cluster: -1
        };
        nodes.push(obj);
        nodes_dict[n] = obj;
      }
    }
    return changed = true;
  };

  add_link = function(list) {
    var e, obj, source, target, _i, _len;
    if (!(list[0] instanceof Array)) {
      list = [list];
    }
    for (_i = 0, _len = list.length; _i < _len; _i++) {
      e = list[_i];
      if (!links_dict[e[0]]) {
        links_dict[e[0]] = Object();
      }
      if (!links_dict[e[0]][e[1]]) {
        source = nodes_dict[e[0]];
        target = nodes_dict[e[1]];
        obj = {
          source: source,
          target: target
        };
        links.push(obj);
        links_dict[e[0]][e[1]] = obj;
      }
    }
    return changed = true;
  };

  remove_node = function(list) {
    var e, obj, _i, _len;
    if (!(list instanceof Array)) {
      list = [list];
    }
    for (_i = 0, _len = list.length; _i < _len; _i++) {
      e = list[_i];
      obj = nodes_dict[e];
      nodes.splice(obj.index, 1);
      delete nodes_dict[e];
    }
    return changed = true;
  };

  remove_link = function(list) {
    var e, i, obj, source, target, _i, _j, _len, _ref;
    if (!(list[0] instanceof Array)) {
      list = [list];
    }
    for (_i = 0, _len = list.length; _i < _len; _i++) {
      e = list[_i];
      source = nodes_dict[e[0]];
      target = nodes_dict[e[1]];
      for (i = _j = 0, _ref = links.length; 0 <= _ref ? _j < _ref : _j > _ref; i = 0 <= _ref ? ++_j : --_j) {
        obj = links[i];
        if (obj.source === source && obj.target === target) {
          links.splice(i, 1);
          break;
        }
      }
      delete links_dict[e[0]][e[1]];
    }
    return changed = true;
  };

  clear = function() {
    while (links.length) {
      links.pop();
    }
    while (nodes.length) {
      nodes.pop();
    }
    nodes_dict = Object();
    window.nodes_dict = nodes_dict;
    return changed = true;
  };

  window.nodes = nodes;

  window.nodes_dict = nodes_dict;

  window.links_dict = links_dict;

  window.links = links;

  window.force = force;

  window.start = start;

  ws = new WebSocket("ws://" + document.location.host + "/websocket/");

  ws.onmessage = function(evt) {
    var msg;
    msg = JSON.parse(evt.data);
    if (msg.action === "add_node") {
      return add_node(msg.nodes);
    } else if (msg.action === "add_link") {
      return add_link(msg.links);
    } else if (msg.action === "remove_node") {
      return remove_node(msg.nodes);
    } else if (msg.action === "remove_link") {
      return remove_link(msg.links);
    } else if (msg.action === "clear") {
      return clear();
    }
  };

}).call(this);

//# sourceMappingURL=graph.map
