width = 1200
height = 1000

color = d3.scale.category10()

nodes = []
nodes_dict = Object()
links = []
links_dict = Object()

tick = () ->
  node #.attr("cx", (d) -> d.x)
       #.attr("cy", (d) -> d.y)
      .attr("transform", (d) -> "translate(" + d.x + "," + d.y + ")")

  link.attr("x1", (d) -> d.source.x )
      .attr("y1", (d) -> d.source.y )
      .attr("x2", (d) -> d.target.x )
      .attr("y2", (d) -> d.target.y )

force = d3.layout.force()
  .nodes(nodes)
  .links(links)
  .charge(-400)
  .linkDistance(120)
  .size([width, height])
  .on("tick", tick)

redraw = () ->
    vis.attr("transform",
             "translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")")

svg = d3.select("body").append("svg")
  .attr("width", width)
  .attr("height", height)
  .call(d3.behavior.zoom().on("zoom", redraw));

log = d3.select("body").append("ul")





vis = svg.append("g")

node = vis.selectAll(".node")
link = vis.selectAll(".link")

changed = false

start = () ->
  if changed
    changed = false
    link = link.data(force.links(),
      (d) -> d.source.id + "-" + d.target.id
    )
    link.enter().insert("line", ".node").attr("class", "link")
    link.exit().remove()

    node = node.data(force.nodes(), (d) -> d.id)
    g = node.enter()
      .append("g").attr("class", (d) -> "node c" + d.cluster)
    g.append("circle").attr("r", 8)
    g.append("text").attr("class", "node-label").attr("dx", 12).attr("dy", ".35em").text((d) -> if d.label then d.label else d.id)
    node.exit().remove()
    force.start()

window.setInterval(start, 100)

#window.setTimeout(
#  () ->
#    a = {id: "a"}
#    b = {id: "b"}
#    c = {id: "c"}
#    nodes.push(a, b, c)
#    links.push({source: a, target: b}, {source: a, target: c}, {source: b, target: c})
#    start()
#  , 0
#)

add_node = (list) ->
  unless list instanceof Array
    list = [list]
  for n in list
    unless nodes_dict[n]
      obj = {id: n, cluster: -1}
      nodes.push(obj)
      nodes_dict[n] = obj
  changed = true
  #start()


add_link = (list) ->
  unless list[0] instanceof Array
    list = [list]
  for e in list
    unless links_dict[e[0]]
      links_dict[e[0]] = Object()
    unless links_dict[e[0]][e[1]]
      source = nodes_dict[e[0]]
      target = nodes_dict[e[1]]
      obj = {source: source, target: target}
      links.push(obj)
      links_dict[e[0]][e[1]] = obj
  #start()
  changed = true


remove_node = (list) ->
  unless list instanceof Array
    list = [list]
  for e in list
    obj = nodes_dict[e]
    nodes.splice(obj.index, 1)
    delete nodes_dict[e]
  #start()
  changed = true


remove_link = (list) ->
  unless list[0] instanceof Array
    list = [list]
  for e in list
    source = nodes_dict[e[0]]
    target = nodes_dict[e[1]]
    for i in [0...links.length]
      obj = links[i]
      if obj.source == source and obj.target==target
        links.splice(i,1)
        break
    delete links_dict[e[0]][e[1]]
  #start()
  changed = true


clear = () ->
  while links.length
    links.pop()
  while nodes.length
    nodes.pop()
  nodes_dict =Object()
  window.nodes_dict = nodes_dict
  #start()
  changed = true

window.nodes = nodes
window.nodes_dict = nodes_dict
window.links_dict = links_dict
window.links = links
window.force = force

window.start = start


ws = new WebSocket("ws://"+document.location.host+"/websocket/")
ws.onmessage = (evt) ->
  msg = JSON.parse(evt.data)
  #log.append("li").text(evt.data)
  if msg.action == "add_node"
    add_node(msg.nodes)
  else if msg.action == "add_link"
    add_link(msg.links)
  else if msg.action == "remove_node"
    remove_node(msg.nodes)
  else if msg.action == "remove_link"
    remove_link(msg.links)
  else if msg.action == "clear"
    clear()
