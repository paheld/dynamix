"""
This modules provides methods for event handling.

Every event is an dictionary in the following form::

    event = {
        "time": [a float value],
        "sender": [a single sender (int or str) or a list of senders],
        "receiver": [a single receiver (int or str) or a list of receivers] (optional),
    }

"""