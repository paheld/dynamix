Introduction
************


Resources
=========

- Git - Repository: http://bitbucket.org/paheld/dynamix
- Documentation: http://dynamix.readthedocs.org


Requirements
============

- SciPy
- NetworkX
- Matplotlib
- twitter
